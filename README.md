# JMeter + Taurus + Jenkins
Proyecto correspondiente al desarrollo de un pipeline para las pruebas de performance en integración continua con las herramientas descritas en el titulo del proyecto.
Para las pruebas realizadas se utilizó Docker, lugar donde se levantó un contenedor con una imagen de Jenkins. La imágen de Jenkins utilizada es `jenkins/jenkins` y el comando utilizado para levantar el contenedor fue:

* `docker run --name jenkins_taurus_jmeter -p 7990:8080 -v "$PWD":/var/jenkins_home jenkins/jenkins`
* `docker run --name jenkins_taurus_jmeter -p 7990:8080 jenkins/jenkins`

Para las pruebas , en la maquina de jenkins se instalo python y pip para posteriormente instalar taurus. Los comandos utilizados para instalar taurus [estan en este link](https://gettaurus.org/install/Installation/)